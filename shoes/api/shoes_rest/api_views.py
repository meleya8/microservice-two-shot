from django.http import JsonResponse
from django.shortcuts import render
from .models import Shoe
from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods
import json

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties= ["name"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "color"]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    # Get a list of shoes, create and delete shoes 
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            )
    else:
        content = json.loads(request.body)
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder= ShoeDetailEncoder,
            safe=False
        )