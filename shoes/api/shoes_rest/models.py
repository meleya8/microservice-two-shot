from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    href = models.CharField(max_length=100)
    closet_name = models.CharField(max_length=100)
    bin_number = models.IntegerField()
    bin_size = models.IntegerField()

class Shoe (models.Model):
    name = models.CharField(max_length=200)
    # brand = models.CharField(max_length=200)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True, blank=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null = True,
        blank=True
    )

    def get_api_url(self):
        return reverse("api_list_shoes")

    