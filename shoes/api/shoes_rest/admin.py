from django.contrib import admin

# Register your models here.
from .models import BinVO

class BinVOAdmin(admin.ModelAdmin):
    pass

admin.site.register(BinVO, BinVOAdmin)
