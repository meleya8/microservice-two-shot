import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatsList from './HatList';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoesForm from './ShoesForm';
import HatsForm from './HatsForm';

function App(props) {
  // if (props.shoes === undefined) {
  //   return null;
  // }
  // if (props.hats === undefined) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className='container'>
        <Routes>
          <Route path='/' element={<MainPage />} />
        </Routes>
        <Routes>
          <Route path='/shoes' element={<ShoesForm />} />
        </Routes>
        <Routes>
          <Route path='/shoes' element={<ShoeList />} />
        </Routes>
        <Routes>
          <Route path='/hats' element={<HatsList />} />
        </Routes>
        <Routes>
          <Route path='/hats' element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

