import React from "react";

class ShoesForm extends React.Component {
    // to fetch data 
    async componentDidMount() { 
        const url = 'http://localhost:8080/api/shoes/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            this.setState({shoes: data.shoes})
        }
    }
    render() {
      return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoes</h1>
            <form id="create-location-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
                <label htmlFor="room_count">Brand</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                <label htmlFor="city">Color</label>
              </div>
              <div className="mb-3">
                <select required name="state" id="state" className="form-select">
                  <option  value="">Size</option>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
    }
  }

export default ShoesForm;
