import React from "react";

function ShoeList(props) {
    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold">SHOESSSS!</h1>
            <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
                Need to keep track of your shoes and hats? We have
                the solution for you!
            </p>
            <div className="nav-item">
                {/* <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink> */}
                {/* <Link to="/" className="btn btn-primary btn-lg px-4 gap-3">Home</Link> */}
            </div>
            {/* <div className="shoes">
                {props.list.map(data => {
                    const shoe = data.shoes;
                    return (
                        <div className="clearfix">
                            <img src="https://img.ssensemedia.com/images/221813F122003_1/christian-louboutin-multicolor-so-kate-120mm-heels.jpg" class="rounded" alt="..." className="col-md-6 float-md-end mb-3 ms-md-3" alt="..."></img>
                            <p> {shoe.name} </p>
                        </div>
                    )
                })} */}
            {/* </div> */}
            <div className="clearfix">
                <img src="https://img.ssensemedia.com/images/221813F122003_1/christian-louboutin-multicolor-so-kate-120mm-heels.jpg" className="rounded" alt="..." className="col-md-6 float-md-end mb-3 ms-md-3" alt="..."></img>
                <p> A paragraph of placeholder text. </p>
            </div>
         </div>
    </div>
    )
}

export default ShoeList;