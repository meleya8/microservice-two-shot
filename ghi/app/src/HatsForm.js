import React from "react";

class HatsForm extends React.Component {
    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';
    
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({hats: data.hats});
        }
    }
    render() {
      return (
        <div class="row">
            <div class="offset-3 col-6">
                <div class="shadow p-4 mt-4">
                    <h1>Create a new Hat</h1>
                    <form id="create-hat-form">
                        <div class="form-floating mb-3">
                            <input placeholder="Style Name" required type="text" name="style_name" id="style_name" class="form-control"/>
                            <label for="style_name">Style Name</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input placeholder="Fabric" required type="text" name="fabric" id="fabric" class="form-control"/>
                            <label for="fabric">Fabric</label>
                        </div>
                        <button class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
      </div>
      );
    }
  }

  export default HatsForm;