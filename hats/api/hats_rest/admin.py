from django.contrib import admin


from .models import Hats, LocationVO

class LocationVOAdmin(admin.ModelAdmin):
    pass
class HatsAdmin(admin.ModelAdmin):
    pass
admin.site.register(LocationVO,LocationVOAdmin)
admin.site.register(Hats,HatsAdmin)
