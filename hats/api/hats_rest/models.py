from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    href =models.CharField(max_length=100)
    closet_name= models.CharField(max_length=100)
    section_number= models.IntegerField()
    shelf_number= models.IntegerField()

class Hats(models.Model):
    
    style_name= models.CharField(max_length=100)
    fabric=models.CharField(max_length=100)
    picture= models.URLField(null=True,blank=True)
    location= models.ForeignKey(LocationVO,related_name="hats",on_delete=models.CASCADE, null=True,blank=True)
    

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_list_hats")

