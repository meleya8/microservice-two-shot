
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hats
from common.json import ModelEncoder

class HatsListEncoder(ModelEncoder):
    model= Hats
    properties = ['style_name']

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = ['fabric']

@require_http_methods(['GET', 'POST'])
def api_list_hats(request):
    if request.method == 'GET':
        hats= Hats.objects.all()
        return JsonResponse(
            {'hats':hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder= HatsDetailEncoder,
            safe= False
        )
      

