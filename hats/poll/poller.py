import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
from hats_rest.models import LocationVO
# from hats_rest.models import Something
# hats_rest.models import

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            url = "http://wardrobe-api:8000/api/locations/"
            response = requests.get(url)
            content = json.loads(response.content)
            print('this is content testing',content)
            for location in content['locations']:
                LocationVO.objects.update_or_create(
                    href=location["href"],
                    defaults={"closet_name": location["closet_name"],
                        'section_number': location['section_number'],
                        'shelf_number': location['shelf_number']
                    }
                )
        except Exception as e:
            print('something has gone wrong',e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()
